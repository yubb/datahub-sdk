<?php

namespace Zlien\DataAccessService\Traits;

/**
 * Trait HasUrlParam
 * @package Zlien\DataAccessService\Traits
 */
trait HasUrlParam
{
    /**
     * @param $values
     * @param $uri
     * @return mixed
     */
    public function replaceParam($values, $uri)
    {
        foreach ($values as $uriParam => $uriParamValue) {
            $uri = str_replace($uriParam, $uriParamValue, $uri);
        }

        return $uri;
    }
}
