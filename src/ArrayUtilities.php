<?php

namespace Zlien\DataAccessService;

/**
 * Class ArrayUtilities
 * @package Zlien\DataAccessService
 */
abstract class ArrayUtilities
{
    /**
     * @param $inputArray
     * @param $fieldKey
     * @param $defaultValue
     * @return mixed
     */
    public static function getArrayField($inputArray, $fieldKey, $defaultValue)
    {
        return empty($inputArray[$fieldKey]) ? $defaultValue : $inputArray[$fieldKey];
    }
}
