<?php

namespace Zlien\DataAccessService\Factories;

use Psr\Http\Message\ResponseInterface;
use Zlien\DataAccessService\Response;

/**
 * Class ResponseFactory
 * @package Zlien\DataAccessService\Factories
 */
class ResponseFactory
{
    /**
     * @param ResponseInterface $endpointResponse
     * @param $parsedResponse
     * @return Response
     */
    public static function create(ResponseInterface $endpointResponse, $parsedResponse)
    {
        // Make sure we have a valid response
        $statusCodePosition = strpos($endpointResponse->getStatusCode(), '2');
        if ($statusCodePosition === false || $statusCodePosition != 0) {
            $parsedResponse = $endpointResponse->getReasonPhrase();
        }

        // Build the response
        $response = new Response();
        $response->setStatusCode($endpointResponse->getStatusCode());
        $response->setContent($parsedResponse);

        return $response;
    }
}
