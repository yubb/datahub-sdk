<?php

namespace Zlien\DataAccessService;

/**
 * Class StringUtilities
 * @package Zlien\DataAccessService
 */
abstract class StringUtilities
{
    /**
     * Cleans string from unwanted/bom chars
     *
     * @param $string
     * @return bool|mixed|string
     */
    public static function removeUnseenCharacters($string)
    {
        // This will remove unwanted characters.
        // Check http://www.php.net/chr for details
        for ($i = 0; $i <= 31; ++$i) {
            $string = str_replace(chr($i), "", $string);
        }
        $string = str_replace(chr(127), "", $string);

        // This is the most common part
        // Some file begins with 'efbbbf' to mark the beginning of the file. (binary level)
        // here we detect it and we remove it, basically it's the first 3 characters
        if (0 === strpos(bin2hex($string), 'efbbbf')) {
            $string = substr($string, 3);
        }

        return $string;
    }
}
