<?php

namespace Zlien\DataAccessService\Dodge;

use Zlien\DataAccessService\Dodge\Endpoints\DodgeReportEndpoint;
use Zlien\DataAccessService\Dodge\Endpoints\DodgeSearchForEndpoint;

/**
 * Class DodgeAPI
 * @package Zlien\DataAccessService\Dodge
 */
class DodgeAPI
{
    /**
     * @var string
     */
    private $baseUrl;

    /**
     * DodgeAPI constructor.
     * @param $baseUrl string must end with a slash.
     */
    public function __construct($baseUrl)
    {
        $this->baseUrl       = $baseUrl;
    }

    /**
     * Calls DODGE Search API with lat,long and specified radius (in miles).
     *
     * @param $latitude
     * @param $longitude
     * @param $radius
     * @param $startTime
     * @param $endTime
     * @param array $options
     *
     * @return \Zlien\DataAccessService\Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public function searchAround($latitude, $longitude, $radius, $startTime = null, $endTime = null, $options = [])
    {
        $endpoint = new DodgeSearchForEndpoint($this->baseUrl);
        $body     = [
            'geo-location' => [
                'latitude'  => $latitude,
                'longitude' => $longitude,
                'radius'    => $radius,
            ]
        ];

        // Add start time and end time to the body
        if (!empty($startTime) || !empty($endTime)) {
            $body['report-date'] = [];

            // If start time is available, add it.
            if (!empty($startTime)) {
                $body['report-date']['start-date-time'] = $startTime;
            }

            // If end time is there, add it
            if (!empty($endTime)) {
                $body['report-date']['end-date-time'] = $endTime;
            }
        }

        // Prepare request's body and headers
        $body = array_merge($body, $options);
        $endpoint->prepareBody($body);

        // Do the request and return the response.
        return $endpoint->execute();
    }

    /**
     * @param       $fullAddress
     * @param null  $state
     * @param null  $city
     * @param null  $county
     * @param null  $zip
     * @param null  $startTime
     * @param null  $endTime
     * @param array $options
     *
     * @return \Zlien\DataAccessService\Response
     * @throws \Exception
     */
    public function searchAddress(
        $fullAddress,
        $state = null,
        $city = null,
        $county = null,
        $zip = null,
        $startTime = null,
        $endTime = null,
        $options = []
    ) {
        $endpoint = new DodgeSearchForEndpoint($this->baseUrl);
        $body     = [
            'search-term' => $fullAddress
        ];
        if ($state) {
            $body['geography'] = [
                'country' => 'USA',
                'state'   => $state,
            ];
        }
        if($county) {
            $body['geography']['county'] = $county;
        }

        // Add start time and end time to the body
        if (!empty($startTime) || !empty($endTime)) {
            $body['report-date'] = [];

            // If start time is available, add it.
            if (!empty($startTime)) {
                $body['report-date']['start-date-time'] = $startTime;
            }

            // If end time is there, add it
            if (!empty($endTime)) {
                $body['report-date']['end-date-time'] = $endTime;
            }
        }

        // Prepare request's body and headers
        $body = array_merge($body, $options);
        $endpoint->prepareBody($body);

        // Do the request and return the response.
        return $endpoint->execute();
    }

    /**
     * Gets the report for a specific project using the report id retrieved from the search endpoint.
     *
     * @param $dodgeReportId
     *
     * @return \Zlien\DataAccessService\Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getReportFor($dodgeReportId)
    {
        $endpoint = new DodgeReportEndpoint($this->baseUrl, $dodgeReportId);

        return $endpoint->execute();
    }
}
