<?php

namespace Zlien\DataAccessService\Dodge\Endpoints;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Zlien\DataAccessService\Dodge\EnumDodgeConfig;
use Zlien\DataAccessService\Factories\ResponseFactory;

/**
 * Class DodgeEndpoint
 * Acts as the base endpoint for all Dodge supported endpoints.
 * @package Zlien\DataAccessService\Dodge\Endpoints
 */
abstract class DodgeEndpoint
{
    /**
     * @var Client
     */
    protected $request;

    /**
     * @var string
     */
    protected $uri;

    /**
     * @var string
     */
    protected $method = 'GET';

    /**
     * @var string
     */
    protected $body = '';

    /**
     * @var array
     */
    protected $headers = [];

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function execute()
    {
        $this->initRequest();
        $endpointResponse = $this->request->request($this->method);
        $parsedResponse   = $this->parse($endpointResponse->getBody()->getContents());

        return ResponseFactory::create($endpointResponse, $parsedResponse);
    }

    /**
     * DodgeEndpoint constructor.
     * @param Request $request
     */
    protected function initRequest()
    {
        $this->request = new Client([
            'base_uri'    => $this->uri,
            'body'        => $this->body,
            'headers'     => $this->headers,
            'http_errors' => false,
        ]);
    }

    /**
     * Filters the input headers if not whitelisted.
     *
     * @param $inputHeaders
     * @return array
     */
    protected function filterHeaders($inputHeaders)
    {
        $filteredHeaders = [];
        foreach ($inputHeaders as $headerKey => $headerValue) {

            //.
            if (in_array($headerKey, EnumDodgeConfig::ALLOWED_HEADERS)) {
                $filteredHeaders[$headerKey] = $headerValue;
            }
        }

        return $filteredHeaders;
    }

    /**
     * Prepares the request headers.
     *
     * @param array $headers
     * @return mixed
     */
    public abstract function prepareHeaders($headers = []);

    /**
     * Prepares the request body.
     *
     * @param array $data
     * @return mixed
     */
    public function prepareBody($data = [])
    {
        $this->body = json_encode($data);
    }

    /**
     * Parses a string into the required classes. (kind of unmarshalling the response)
     *
     * @param $content
     * @return mixed
     */
    public abstract function parse($content);
}
