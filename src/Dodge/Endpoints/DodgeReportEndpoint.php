<?php

namespace Zlien\DataAccessService\Dodge\Endpoints;

use Zlien\DataAccessService\Dodge\EnumDodgeConfig;
use Zlien\DataAccessService\Dodge\Parsers\ReportEndpointParser;
use Zlien\DataAccessService\Traits\HasUrlParam;

/**
 * Class DodgeReportEndpoint
 * @package Zlien\DataAccessService\Dodge\Endpoints
 */
class DodgeReportEndpoint extends DodgeEndpoint
{
    use HasUrlParam;

    /**
     * DodgeReportEndpoint constructor.
     * @param $baseUrl
     * @param $reportId
     */
    public function __construct($baseUrl, $reportId)
    {
        $this->method = 'GET';

        // Initialize default body and headers values.
        $this->initUri($baseUrl, $reportId);
        $this->initHeaders();
    }

    /**
     * Prepares the request headers.
     *
     * @param array $headers
     * @return mixed
     */
    public function prepareHeaders($headers = [])
    {
        $this->headers = array_merge($this->headers, $headers);
    }

    /**
     * Parses a string into the required classes. (kind of unmarshalling the response)
     *
     * @param $content
     * @return mixed
     */
    public function parse($content)
    {
        $parser = new ReportEndpointParser();

        try {
            return $parser->parse($content);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param $baseUrl
     * @param $reportId
     */
    private function initUri($baseUrl, $reportId)
    {
        $uri = $baseUrl . 'report/{report-id}';
        $this->uri = $this->replaceParam([
            '{report-id}' => $reportId
        ], $uri);
    }

    /**
     * Default headers
     */
    private function initHeaders()
    {
        $this->headers = [
            'Content-Type'  => 'application/json',
            'Cache-Control' => 'no-cache',
        ];
    }
}
