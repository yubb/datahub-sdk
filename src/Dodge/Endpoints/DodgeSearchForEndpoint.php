<?php

namespace Zlien\DataAccessService\Dodge\Endpoints;

use Zlien\DataAccessService\Dodge\EnumDodgeConfig;
use Zlien\DataAccessService\Dodge\Parsers\SearchForEndpointParser;

/**
 * Class DodgeSearchForEndpoint
 * Responsible for calling the dodge endpoint.
 * @package Zlien\DataAccessService\Dodge\Endpoints
 */
class DodgeSearchForEndpoint extends DodgeEndpoint
{
    /**
     * DodgeSearchForEndpoint constructor.
     * @param $baseUrl
     * @throws \Exception
     */
    public function __construct($baseUrl)
    {
        $this->method = 'POST';
        $this->uri    = $baseUrl . 'projects/search';

        // Initialize default body and headers values.
        $this->initBody();
        $this->initHeaders();
    }

    /**
     * Prepares the request headers.
     *
     * @param array $headers
     * @return mixed
     */
    public function prepareHeaders($headers = [])
    {
        $filteredHeaders = $this->filterHeaders($headers);
        $this->headers   = array_merge($this->headers, $filteredHeaders);
    }

    /**
     * Prepares the request body.
     *
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function prepareBody($data = [])
    {
        $this->validateBody($data);
        $this->body['criteria'] = array_merge($this->body['criteria'], $data);
        $this->body = json_encode($this->body);
    }

    /**
     * Parses a string into the required classes. (kind of unmarshalling the response)
     *
     * @param $content
     * @return mixed
     * @throws \Exception
     */
    public function parse($content)
    {
        $parser = new SearchForEndpointParser();

        try {
            return $parser->parse($content);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Default headers
     */
    private function initHeaders()
    {
        $this->headers = [
            'Content-Type'  => 'application/json',
            'Cache-Control' => 'no-cache',
        ];
    }

    /**
     * Default body
     * @throws \Exception
     */
    private function initBody()
    {
        $this->body = [
            'criteria' => [
                'report-date' => [
                    'start-date-time' => $this->getStartDateTime(),
                    'end-date-time'   => $this->getEndDateTime(),
                ],
                'search-result-page-info' => [
                    'start-index' => 1,
                    'page-size'   => 25,
                ]
            ]
        ];
    }

    /**
     * Provides default start date time.
     *
     * @return string
     * @throws \Exception
     */
    private function getStartDateTime()
    {
        $startDateTime = (new \DateTime('now'));
        $startDateTime->sub(new \DateInterval('P2Y'));

        return $startDateTime->format('Y-m-d\\T00:00:00');
    }

    /**
     * Provides default end date time.
     *
     * @return string
     * @throws \Exception
     */
    private function getEndDateTime()
    {
        $endDateTime = (new \DateTime('now'));
        $endDateTime->add(new \DateInterval('P2Y'));

        return $endDateTime->format('Y-m-d\\T00:00:00');
    }

    /**
     * Validates the body.
     *
     * @param $data
     * @throws \Exception
     */
    private function validateBody($data)
    {
        if (empty($data['geo-location']) && (empty($data['geography']) && empty($data['search-term']))) {
            throw new \Exception(
                sprintf('Key %s or keys %s are required in body.', '`geo-location`', '`geography` and `search-term`')
            );
        }
    }
}
