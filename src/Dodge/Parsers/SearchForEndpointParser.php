<?php

namespace Zlien\DataAccessService\Dodge\Parsers;

use Zlien\DataAccessService\Dodge\DTOs\DodgeProjectSearchResultItemGeolocationMetaDTO;
use Zlien\DataAccessService\Dodge\DTOs\DodgeErrorDTO;
use Zlien\DataAccessService\Dodge\DTOs\DodgeProjectSearchResultDTO;
use Zlien\DataAccessService\Dodge\DTOs\DodgeProjectSearchResultItemMetaDTO;
use Zlien\DataAccessService\Dodge\DTOs\DodgeSearchResultDTO;
use Zlien\DataAccessService\Dodge\DTOs\DodgeSearchResultItemDTO;
use Zlien\DataAccessService\StringUtilities;

/**
 * Class SearchForEndpointParser
 * @package Zlien\DataAccessService\Dodge\Parsers
 */
class SearchForEndpointParser
{
    /**
     * @param $content
     * @return DodgeSearchResultDTO
     * @throws \Exception
     */
    public function parse($content)
    {
        $content = json_decode(StringUtilities::removeUnseenCharacters($content), true);
        if (empty($content['response'])) {
            throw new \Exception('Invalid response');
        }
        $content = $content['response'];

        // Base result
        $result = new DodgeSearchResultDTO();

        // Error
        $this->buildError($content, $result);

        // Project search results
        $this->buildResponseInformation($content, $result);

        // Project search results items
        $this->buildResults($content, $result);

        return $result;
    }

    /**
     * @param $content
     * @param $result
     */
    private function buildError($content, DodgeSearchResultDTO $result)
    {
        if (empty($content['errors'])) {
            return;
        }
        $error = new DodgeErrorDTO();
        $error->setErrorCode($content['errors']['error-code']);
        $error->setErrorMessage($content['errors']['error-message']);
        $result->setError($error);
    }

    /**
     * @param $content
     * @param $result
     */
    private function buildResponseInformation($content, DodgeSearchResultDTO $result)
    {
        if (empty($content['dodge-project-search-results'])) {
            return;
        }
        $projectSearchResult = new DodgeProjectSearchResultDTO();
        $projectSearchResult->setStartIndex($content['dodge-project-search-results']['start-index']);
        $projectSearchResult->setTotal($content['dodge-project-search-results']['total']);
        $projectSearchResult->setEndIndex($content['dodge-project-search-results']['end-index']);
        $projectSearchResult->setFacetItems($content['dodge-project-search-results']['facet-items']);
        $result->setProjectSearchResult($projectSearchResult);
    }

    /**
     * @param $content
     * @param $result
     */
    private function buildResults($content, DodgeSearchResultDTO $result)
    {
        $resultItems = [];
        if (
            empty($content['dodge-project-search-results']['result-items'])
            || empty($content['dodge-project-search-results']['result-items'])
        ) {
            // Do nothing.
            return;
        }

        // Loop on result items, and build each of them.
        foreach ($content['dodge-project-search-results']['result-items']['result-item'] as $resultItemKey => $metaData) {
            $metaData = $metaData['result-item-data'];
            $resultMetaItems = [];
            foreach ($metaData as $index => $metaDataItem) {
                $item = $this->buildResultItemMetaData($metaDataItem);
                $resultMetaItems[$index] = $item;
            }
            $resultItem = new DodgeSearchResultItemDTO();
            $resultItem->setMetaData($resultMetaItems);
            $resultItems[] = $resultItem;
        }
        $result->getProjectSearchResult()->setResultItems($resultItems);
    }

    /**
     * @param $metaDataItem
     * @return DodgeProjectSearchResultItemGeolocationMetaDTO|DodgeProjectSearchResultItemMetaDTO
     */
    private function buildResultItemMetaData($metaDataItem)
    {
        $key = $metaDataItem['@key'];
        if ($key == 'p-geocode') {
            return $this->buildResultItemMetaDataForGeolocation($metaDataItem);
        }
        $item = new DodgeProjectSearchResultItemMetaDTO();
        $item->setKey($key);
        $item->setText(\Zlien\DataAccessService\ArrayUtilities::getArrayField($metaDataItem, '#text', ''));

        return $item;
    }

    /**
     * @param $metaDataItem
     * @return DodgeProjectSearchResultItemGeolocationMetaDTO
     */
    private function buildResultItemMetaDataForGeolocation($metaDataItem)
    {
        $item = new DodgeProjectSearchResultItemGeolocationMetaDTO();
        $item->setProjectGeocodeLocationType($metaDataItem['project-geocode-location-type']);
        $item->setProjectGeocodeType($metaDataItem['project-geocode-type']);
        $item->setProjectLatitude($metaDataItem['project-latitude']);
        $item->setProjectLongitude($metaDataItem['project-longitude']);

        return $item;
    }
}
