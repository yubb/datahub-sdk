<?php

namespace Zlien\DataAccessService\Dodge\Parsers;

use Zlien\DataAccessService\ArrayUtilities;
use Zlien\DataAccessService\Dodge\DTOs\AddressLineDTO;
use Zlien\DataAccessService\Dodge\DTOs\ContactInformationDTO;
use Zlien\DataAccessService\Dodge\DTOs\ContactRoleDTO;
use Zlien\DataAccessService\Dodge\DTOs\DodgeErrorDTO;
use Zlien\DataAccessService\Dodge\DTOs\DodgeReportDataDTO;
use Zlien\DataAccessService\Dodge\DTOs\DodgeReportDTO;
use Zlien\DataAccessService\Dodge\DTOs\DodgeReportGeocodeDTO;
use Zlien\DataAccessService\Dodge\DTOs\DodgeReportProjectLocationDTO;
use Zlien\DataAccessService\Dodge\DTOs\DodgeReportResultDTO;
use Zlien\DataAccessService\Dodge\DTOs\DodgeReportSummaryDTO;
use Zlien\DataAccessService\Dodge\DTOs\ProjectContactDTO;
use Zlien\DataAccessService\Dodge\DTOs\ProjectContactInformationDTO;
use Zlien\DataAccessService\StringUtilities;

/**
 * Class ReportEndpointParser
 * @package Zlien\DataAccessService\Dodge\Parsers
 */
class ReportEndpointParser
{
    /**
     * @param $content
     * @return DodgeReportResultDTO
     * @throws \Exception
     */
    public function parse($content)
    {
        $content = json_decode(StringUtilities::removeUnseenCharacters($content), true);
        if (empty($content['response'])) {
            throw new \Exception('Invalid response');
        }
        $content = $content['response'];

        // Base Result
        $result = new DodgeReportResultDTO();

        // Build response
        $this->buildError($content, $result);
        $this->buildReport($content, $result);
        $this->buildGeocode($content, $result);

        return $result;
    }

    /**
     * @param $content
     * @param DodgeReportResultDTO $result
     */
    private function buildGeocode($content, DodgeReportResultDTO $result)
    {
        if (empty($content['reports']['report']['p-geocode'])) {
            return;
        }
        $geocode = new DodgeReportGeocodeDTO();
        $geocodeData = $content['reports']['report']['p-geocode']['title-code'];
        $geocode->setLatitude($geocodeData['latitude']);
        $geocode->setLongitude($geocodeData['longitude']);
        $geocode->setType($geocodeData['location-type']);
        $result->setGeoCode($geocode);
    }

    /**
     * @param $content
     * @param DodgeReportResultDTO $result
     */
    public function buildReport($content, DodgeReportResultDTO $result)
    {
        if ($this->reportExists($content)) {
            return;
        }
        $report = new DodgeReportDTO();
        $this->buildReportSummary($content['reports']['report']['dodge-report']['summary'], $report);
        $this->buildReportData($content['reports']['report']['dodge-report']['data'], $report);
        $result->setReport($report);
    }

    /**
     * @param $content
     * @param DodgeReportDTO $report
     */
    public function buildReportSummary($content, DodgeReportDTO $report)
    {
        $summary = new DodgeReportSummaryDTO();
        $summary->setReportNumber(ArrayUtilities::getArrayField($content, 'dr-nbr', null));
        $summary->setReportVersion(ArrayUtilities::getArrayField($content, 'dr-ver', null));
        $summary->setReportType(ArrayUtilities::getArrayField($content, 'report-type', null));
        $summary->setSubProjectCount(ArrayUtilities::getArrayField($content, 'sub-proj-count', null));
        $summary->setPublishDate(ArrayUtilities::getArrayField($content, 'publish-date', null));
        $summary->setFirstPublishDate(ArrayUtilities::getArrayField($content, 'first-publish-date', null));
        $summary->setPublisher(ArrayUtilities::getArrayField($content, 'publisher', null));
        $summary->setCreator(ArrayUtilities::getArrayField($content, 'creator', null));
        $summary->setCopyright(ArrayUtilities::getArrayField($content, 'copyright', null));
        $summary->setCnProjectUrl(ArrayUtilities::getArrayField($content, 'cn-project-url', null));
        $report->setSummary($summary);
    }

    /**
     * @param $content
     * @param DodgeReportDTO $report
     */
    public function buildReportData($content, DodgeReportDTO $report)
    {
        $reportData = new DodgeReportDataDTO();
        $reportData->setProjectTitle($content['proj-title']['title-code']['title']['#text']);
        $reportData->setProjectValuationCurrency($content['project-valuation']['title-code']['currency-type']);
        $reportData->setProjectValuationValue($content['project-valuation']['title-code']['est-low']['#text']);
        $reportData->setProjectStage($content['project-stage']['title-code']['stage-desc']['#text']);
        $reportData->setProjectWorkType($content['project-work-type']['title-code']['work-type']['#text']);
        $reportData->setProjectSquareFootage($content['details']['structural-data']['title-code']['square-footage']['#text']);
        $reportData->setProjectUOM($content['details']['structural-data']['title-code']['square-footage-uom']['#text']);
        $reportData->setProjectType($content['project-type']['title-code']['proj-type']['#text']);
        $report->setData($reportData);

        // Add location
        $this->buildLocation($content['p-location']['project-location']['title-code'], $report);

        // Add Project Contact Information
        $this->buildProjectContactInformation($content['project-contact-information'], $report);
    }

    /**
     * @param $content
     * @param DodgeReportDTO $report
     */
    private function buildLocation($content, DodgeReportDTO $report)
    {
        $location = new DodgeReportProjectLocationDTO();
        $location->setAddressLine1($content['p-addr-line']['p-addr-line1']['#text']);
        $location->setProjectCounty($content['p-county-name']['#text']);
        $location->setFipsCounty(ArrayUtilities::getArrayField($content, 'p-fips-county', null));
        $location->setCityName($content['p-city-name']['#text']);
        $location->setStateId($content['p-state-id']['#text']);
        $location->setZipCode($content['p-zip-code']['#text']);
        $location->setZipCode5($content['p-zip-code5']['#text']);
        $location->setCountryId($content['p-country-id']['#text']);
        $report->getData()->setLocation($location);
    }

    /**
     * @param $content
     * @param DodgeReportDTO $report
     */
    private function buildProjectContactInformation($content, DodgeReportDTO $report)
    {
        $projectContactInformation = new ProjectContactInformationDTO();
        $this->buildProjectContacts($content, $projectContactInformation);
        $report->getData()->setProjectContactInformation($projectContactInformation);
    }

    /**
     * @param $content
     * @param $result
     */
    private function buildError($content, DodgeReportResultDTO $result)
    {
        if (empty($content['errors'])) {
            return;
        }
        $error = new DodgeErrorDTO();
        $error->setErrorCode($content['errors']['error-code']);
        $error->setErrorMessage($content['errors']['error-message']);
        $result->setError($error);
    }

    /**
     * @param $content
     * @param ProjectContactInformationDTO $result
     */
    public function buildProjectContacts($content, ProjectContactInformationDTO $result)
    {
        if (empty($content['title-code']) || empty($content['title-code']['project-contact'])) {
            $result->setProjectContacts([]);
            return;
        }

        $projectContacts = [];
        foreach ($content['title-code']['project-contact'] as $projectContactData) {
            $projectContact = $this->buildProjectContact($projectContactData);
            $projectContacts[] = $projectContact;
        }
        $result->setProjectContacts($projectContacts);
    }

    /**
     * @param $projectContactData
     * @return ProjectContactDTO
     */
    public function buildProjectContact($projectContactData)
    {
        $projectContact = new ProjectContactDTO();
        $projectContact->setContactRole($this->buildContactRole($projectContactData['contact-role']));
        $projectContact->setContactInformation($this->buildContactInformation($projectContactData));

        return $projectContact;
    }

    /**
     * @param $contactRoleData
     * @return null|ContactRoleDTO
     */
    public function buildContactRole($contactRoleData)
    {
        if (empty($contactRoleData)) {
            return null;
        }

        $contactRole = new ContactRoleDTO();
        $contactRole->setCode($contactRoleData['@code']);
        $contactRole->setText($contactRoleData['#text']);

        return $contactRole;
    }

    /**
     * @param $projectContactData
     * @return null|ContactInformationDTO
     */
    public function buildContactInformation($projectContactData)
    {
        if (empty($projectContactData['contact-information'])) {
            return null;
        }
        $inputContactInformation = $projectContactData['contact-information'];
        $contactInformation = new ContactInformationDTO();
        $contactInformation->setContactCategory(ArrayUtilities::getArrayField($inputContactInformation, 's-contact-category', ''));
        $contactInformation->setContactGroup(ArrayUtilities::getArrayField($inputContactInformation, 's-contact-group', ''));
        $contactInformation->setFirmName(ArrayUtilities::getArrayField($inputContactInformation, 'firm-name', ''));
        $contactInformation->setCityName(ArrayUtilities::getArrayField($inputContactInformation, 'c-city-name', ''));
        $contactInformation->setStateId(ArrayUtilities::getArrayField($inputContactInformation, 'c-state-id', ''));
        $contactInformation->setZipCode(ArrayUtilities::getArrayField($inputContactInformation, 'c-zip-code', ''));
        $contactInformation->setZipCode5(ArrayUtilities::getArrayField($inputContactInformation, 'c-zip-code5', ''));
        $contactInformation->setCountryId(ArrayUtilities::getArrayField($inputContactInformation, 'c-country-id', ''));
        $contactInformation->setAreaCode(ArrayUtilities::getArrayField($inputContactInformation, 'area-code', ''));
        $contactInformation->setPhoneNumber(ArrayUtilities::getArrayField($inputContactInformation, 'phone-nbr', ''));
        $contactInformation->setDcisFactorCode(ArrayUtilities::getArrayField($inputContactInformation, 'dcis-factor-code', ''));
        $contactInformation->setDcisFactorContactCode(ArrayUtilities::getArrayField($inputContactInformation, 'dcis-factor-cntct-code', ''));

        // Fill in the contact role
        if (!empty(ArrayUtilities::getArrayField($inputContactInformation, 's-contact-role', ''))) {
            $contactInformation->setContactRole(
                $this->buildContactRole(ArrayUtilities::getArrayField($inputContactInformation, 's-contact-role', ''))
            );
        }

        // Fill in the address line
        if (!empty(ArrayUtilities::getArrayField($inputContactInformation, 'c-addr-line', ''))) {
            $contactInformation->setAddressLine($this->buildAddressLine($projectContactData));
        }

        return $contactInformation;
    }

    /**
     * @param $projectContactData
     * @return AddressLineDTO
     */
    public function buildAddressLine($projectContactData)
    {
         $addressLine = new AddressLineDTO();
         $addressLine->setAddressLine1(ArrayUtilities::getArrayField($projectContactData['contact-information'], 'c-addr-line', '')['c-addr-line1']);

         return $addressLine;
    }

    /**
     * @param $content
     * @return bool
     */
    private function reportExists($content)
    {
        return empty($content['reports']) || empty($content['reports']['report']) || empty($content['reports']['report']['dodge-report']);
    }
}
