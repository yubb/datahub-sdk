<?php

namespace Zlien\DataAccessService\Dodge\DTOs;

/**
 * Class DodgeProjectSearchResultDTO
 * @package Zlien\DataAccessService\Dodge\DTOs
 */
class DodgeProjectSearchResultDTO
{
    /**
     * @var string
     */
    public $total;

    /**
     * @var string
     */
    public $startIndex;

    /**
     * @var string
     */
    public $endIndex;

    /**
     * @var array
     */
    public $facetItems;

    /**
     * @var array
     */
    public $resultItems;

    /**
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param string $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return string
     */
    public function getStartIndex()
    {
        return $this->startIndex;
    }

    /**
     * @param string $startIndex
     */
    public function setStartIndex($startIndex)
    {
        $this->startIndex = $startIndex;
    }

    /**
     * @return string
     */
    public function getEndIndex()
    {
        return $this->endIndex;
    }

    /**
     * @param string $endIndex
     */
    public function setEndIndex($endIndex)
    {
        $this->endIndex = $endIndex;
    }

    /**
     * @return array
     */
    public function getFacetItems()
    {
        return $this->facetItems;
    }

    /**
     * @param array $facetItems
     */
    public function setFacetItems($facetItems)
    {
        $this->facetItems = $facetItems;
    }

    /**
     * @return array
     */
    public function getResultItems()
    {
        return $this->resultItems;
    }

    /**
     * @param array $resultItems
     */
    public function setResultItems($resultItems)
    {
        $this->resultItems = $resultItems;
    }
}
