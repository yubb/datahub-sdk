<?php

namespace Zlien\DataAccessService\Dodge\DTOs;

/**
 * Class DodgeReportDataDTO
 * @package Zlien\DataAccessService\Dodge\DTOs
 */
class DodgeReportDataDTO
{
    /**
     * @var string
     */
    public $projectTitle;

    /**
     * @var DodgeReportProjectLocationDTO
     */
    public $location;

    /**
     * @var string
     */
    public $projectValuationCurrency;

    /**
     * @var string
     */
    public $projectValuationValue;

    /**
     * @var string
     */
    public $projectType;

    /**
     * @var string
     */
    public $projectStage;

    /**
     * @var string
     */
    public $projectWorkType;

    /**
     * @var string
     */
    public $projectSquareFootage;

    /**
     * @var string
     */
    public $projectUOM;

    /**
     * @var ProjectContactInformationDTO
     */
    public $projectContactInformation;

    /**
     * @return mixed
     */
    public function getProjectTitle()
    {
        return $this->projectTitle;
    }

    /**
     * @param mixed $projectTitle
     */
    public function setProjectTitle($projectTitle)
    {
        $this->projectTitle = $projectTitle;
    }

    /**
     * @return DodgeReportProjectLocationDTO
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param DodgeReportProjectLocationDTO $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * @return string
     */
    public function getProjectValuationCurrency()
    {
        return $this->projectValuationCurrency;
    }

    /**
     * @param string $projectValuationCurrency
     */
    public function setProjectValuationCurrency($projectValuationCurrency)
    {
        $this->projectValuationCurrency = $projectValuationCurrency;
    }

    /**
     * @return string
     */
    public function getProjectValuationValue()
    {
        return $this->projectValuationValue;
    }

    /**
     * @param string $projectValuationValue
     */
    public function setProjectValuationValue($projectValuationValue)
    {
        $this->projectValuationValue = $projectValuationValue;
    }

    /**
     * @return string
     */
    public function getProjectType()
    {
        return $this->projectType;
    }

    /**
     * @param string $projectType
     */
    public function setProjectType($projectType)
    {
        $this->projectType = $projectType;
    }

    /**
     * @return string
     */
    public function getProjectStage()
    {
        return $this->projectStage;
    }

    /**
     * @param string $projectStage
     */
    public function setProjectStage($projectStage)
    {
        $this->projectStage = $projectStage;
    }

    /**
     * @return string
     */
    public function getProjectWorkType()
    {
        return $this->projectWorkType;
    }

    /**
     * @param string $projectWorkType
     */
    public function setProjectWorkType($projectWorkType)
    {
        $this->projectWorkType = $projectWorkType;
    }

    /**
     * @return string
     */
    public function getProjectSquareFootage()
    {
        return $this->projectSquareFootage;
    }

    /**
     * @param string $projectSquareFootage
     */
    public function setProjectSquareFootage($projectSquareFootage)
    {
        $this->projectSquareFootage = $projectSquareFootage;
    }

    /**
     * @return string
     */
    public function getProjectUOM()
    {
        return $this->projectUOM;
    }

    /**
     * @param string $projectUOM
     */
    public function setProjectUOM($projectUOM)
    {
        $this->projectUOM = $projectUOM;
    }

    /**
     * @return ProjectContactInformationDTO
     */
    public function getProjectContactInformation()
    {
        return $this->projectContactInformation;
    }

    /**
     * @param ProjectContactInformationDTO $projectContactInformation
     */
    public function setProjectContactInformation($projectContactInformation)
    {
        $this->projectContactInformation = $projectContactInformation;
    }
}
