<?php

namespace Zlien\DataAccessService\Dodge\DTOs;

/**
 * Class ProjectContactDTO
 * @package Zlien\DataAccessService\Dodge\DTOs
 */
class ProjectContactDTO
{
    /**
     * @var ContactRoleDTO
     */
    public $contactRole;

    /**
     * @var ContactInformationDTO
     */
    public $contactInformation;

    /**
     * @return ContactRoleDTO
     */
    public function getContactRole()
    {
        return $this->contactRole;
    }

    /**
     * @param ContactRoleDTO $contactRole
     */
    public function setContactRole($contactRole)
    {
        $this->contactRole = $contactRole;
    }

    /**
     * @return ContactInformationDTO
     */
    public function getContactInformation()
    {
        return $this->contactInformation;
    }

    /**
     * @param ContactInformationDTO $contactInformation
     */
    public function setContactInformation($contactInformation)
    {
        $this->contactInformation = $contactInformation;
    }
}
