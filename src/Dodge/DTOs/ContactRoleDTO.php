<?php

namespace Zlien\DataAccessService\Dodge\DTOs;

/**
 * Class ContactRoleDTO
 * @package Zlien\DataAccessService\Dodge\DTOs
 */
class ContactRoleDTO
{
    /**
     * @var string
     */
    public $code;

    /**
     * @var string
     */
    public $text;

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }
}
