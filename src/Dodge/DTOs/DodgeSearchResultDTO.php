<?php

namespace Zlien\DataAccessService\Dodge\DTOs;

/**
 * Class DodgeSearchResultDTO
 * @package Zlien\DataAccessService\Dodge\DTOs
 */
class DodgeSearchResultDTO
{
    /**
     * @var DodgeErrorDTO
     */
    public $error;

    /**
     * @var DodgeProjectSearchResultDTO
     */
    public $projectSearchResult;

    /**
     * @return DodgeErrorDTO
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param DodgeErrorDTO $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }

    /**
     * @return DodgeProjectSearchResultDTO
     */
    public function getProjectSearchResult()
    {
        return $this->projectSearchResult;
    }

    /**
     * @param DodgeProjectSearchResultDTO $projectSearchResult
     */
    public function setProjectSearchResult($projectSearchResult)
    {
        $this->projectSearchResult = $projectSearchResult;
    }
}
