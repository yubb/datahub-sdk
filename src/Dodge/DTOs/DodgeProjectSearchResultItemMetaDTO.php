<?php

namespace Zlien\DataAccessService\Dodge\DTOs;

/**
 * Class DodgeProjectSearchResultItemMetaDTO
 * @package Zlien\DataAccessService\Dodge\DTOs
 */
class DodgeProjectSearchResultItemMetaDTO
{
    /**
     * @var string
     */
    public $key;

    /**
     * @var string
     */
    public $text;

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }
}
