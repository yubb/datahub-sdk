<?php

namespace Zlien\DataAccessService\Dodge\DTOs;

/**
 * Class DodgeReportSummaryDTO
 * @package Zlien\DataAccessService\Dodge\DTOs
 */
class DodgeReportSummaryDTO
{
    /**
     * @var string
     */
    public $reportNumber;

    /**
     * @var string
     */
    public $reportVersion;

    /**
     * @var string
     */
    public $reportType;

    /**
     * @var string
     */
    public $subProjectCount;

    /**
     * @var string
     */
    public $publishDate;

    /**
     * @var string
     */
    public $firstPublishDate;

    /**
     * @var string
     */
    public $publisher;

    /**
     * @var string
     */
    public $creator;

    /**
     * @var string
     */
    public $copyright;

    /**
     * @var string
     */
    public $cnProjectUrl;

    /**
     * @return string
     */
    public function getReportNumber()
    {
        return $this->reportNumber;
    }

    /**
     * @param string $reportNumber
     */
    public function setReportNumber($reportNumber)
    {
        $this->reportNumber = $reportNumber;
    }

    /**
     * @return string
     */
    public function getReportVersion()
    {
        return $this->reportVersion;
    }

    /**
     * @param string $reportVersion
     */
    public function setReportVersion($reportVersion)
    {
        $this->reportVersion = $reportVersion;
    }

    /**
     * @return string
     */
    public function getReportType()
    {
        return $this->reportType;
    }

    /**
     * @param string $reportType
     */
    public function setReportType($reportType)
    {
        $this->reportType = $reportType;
    }

    /**
     * @return string
     */
    public function getSubProjectCount()
    {
        return $this->subProjectCount;
    }

    /**
     * @param string $subProjectCount
     */
    public function setSubProjectCount($subProjectCount)
    {
        $this->subProjectCount = $subProjectCount;
    }

    /**
     * @return string
     */
    public function getPublishDate()
    {
        return $this->publishDate;
    }

    /**
     * @param string $publishDate
     */
    public function setPublishDate($publishDate)
    {
        $this->publishDate = $publishDate;
    }

    /**
     * @return string
     */
    public function getFirstPublishDate()
    {
        return $this->firstPublishDate;
    }

    /**
     * @param string $firstPublishDate
     */
    public function setFirstPublishDate($firstPublishDate)
    {
        $this->firstPublishDate = $firstPublishDate;
    }

    /**
     * @return string
     */
    public function getPublisher()
    {
        return $this->publisher;
    }

    /**
     * @param string $publisher
     */
    public function setPublisher($publisher)
    {
        $this->publisher = $publisher;
    }

    /**
     * @return string
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param string $creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
    }

    /**
     * @return string
     */
    public function getCopyright()
    {
        return $this->copyright;
    }

    /**
     * @param string $copyright
     */
    public function setCopyright($copyright)
    {
        $this->copyright = $copyright;
    }

    /**
     * @return string
     */
    public function getCnProjectUrl()
    {
        return $this->cnProjectUrl;
    }

    /**
     * @param string $cnProjectUrl
     */
    public function setCnProjectUrl($cnProjectUrl)
    {
        $this->cnProjectUrl = $cnProjectUrl;
    }
}
