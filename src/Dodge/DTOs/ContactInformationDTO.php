<?php

namespace Zlien\DataAccessService\Dodge\DTOs;

/**
 * Class ContactInformationDTO
 * @package Zlien\DataAccessService\Dodge\DTOs
 */
class ContactInformationDTO
{
    /**
     * @var string
     */
    public $contactRole;

    /**
     * @var string
     */
    public $contactCategory;

    /**
     * @var string
     */
    public $contactGroup;

    /**
     * @var string
     */
    public $firmName;

    /**
     * @var string
     */
    public $cityName;

    /**
     * @var string
     */
    public $stateId;

    /**
     * @var string
     */
    public $zipCode;

    /**
     * @var string
     */
    public $zipCode5;

    /**
     * @var string
     */
    public $countryId;

    /**
     * @var string
     */
    public $areaCode;

    /**
     * @var string
     */
    public $phoneNumber;

    /**
     * @var string
     */
    public $dcisFactorCode;

    /**
     * @var string
     */
    public $dcisFactorContactCode;

    /**
     * @var AddressLineDTO
     */
    public $addressLine;

    /**
     * @return string
     */
    public function getContactRole()
    {
        return $this->contactRole;
    }

    /**
     * @param string $contactRole
     */
    public function setContactRole($contactRole)
    {
        $this->contactRole = $contactRole;
    }

    /**
     * @return string
     */
    public function getContactCategory()
    {
        return $this->contactCategory;
    }

    /**
     * @param string $contactCategory
     */
    public function setContactCategory($contactCategory)
    {
        $this->contactCategory = $contactCategory;
    }

    /**
     * @return string
     */
    public function getContactGroup()
    {
        return $this->contactGroup;
    }

    /**
     * @param string $contactGroup
     */
    public function setContactGroup($contactGroup)
    {
        $this->contactGroup = $contactGroup;
    }

    /**
     * @return string
     */
    public function getFirmName()
    {
        return $this->firmName;
    }

    /**
     * @param string $firmName
     */
    public function setFirmName($firmName)
    {
        $this->firmName = $firmName;
    }

    /**
     * @return string
     */
    public function getCityName()
    {
        return $this->cityName;
    }

    /**
     * @param string $cityName
     */
    public function setCityName($cityName)
    {
        $this->cityName = $cityName;
    }

    /**
     * @return string
     */
    public function getStateId()
    {
        return $this->stateId;
    }

    /**
     * @param string $stateId
     */
    public function setStateId($stateId)
    {
        $this->stateId = $stateId;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param string $zipCode
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
    }

    /**
     * @return string
     */
    public function getZipCode5()
    {
        return $this->zipCode5;
    }

    /**
     * @param string $zipCode5
     */
    public function setZipCode5($zipCode5)
    {
        $this->zipCode5 = $zipCode5;
    }

    /**
     * @return string
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * @param string $countryId
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;
    }

    /**
     * @return string
     */
    public function getAreaCode()
    {
        return $this->areaCode;
    }

    /**
     * @param string $areaCode
     */
    public function setAreaCode($areaCode)
    {
        $this->areaCode = $areaCode;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return string
     */
    public function getDcisFactorCode()
    {
        return $this->dcisFactorCode;
    }

    /**
     * @param string $dcisFactorCode
     */
    public function setDcisFactorCode($dcisFactorCode)
    {
        $this->dcisFactorCode = $dcisFactorCode;
    }

    /**
     * @return string
     */
    public function getDcisFactorContactCode()
    {
        return $this->dcisFactorContactCode;
    }

    /**
     * @param string $dcisFactorContactCode
     */
    public function setDcisFactorContactCode($dcisFactorContactCode)
    {
        $this->dcisFactorContactCode = $dcisFactorContactCode;
    }

    /**
     * @return AddressLineDTO
     */
    public function getAddressLine()
    {
        return $this->addressLine;
    }

    /**
     * @param AddressLineDTO $addressLine
     */
    public function setAddressLine($addressLine)
    {
        $this->addressLine = $addressLine;
    }
}
