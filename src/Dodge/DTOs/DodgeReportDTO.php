<?php

namespace Zlien\DataAccessService\Dodge\DTOs;

class DodgeReportDTO
{
    /**
     * @var DodgeReportSummaryDTO
     */
    public $summary;

    /**
     * @var DodgeReportDataDTO
     */
    public $data;

    /**
     * @return DodgeReportSummaryDTO
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * @param DodgeReportSummaryDTO $summary
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;
    }

    /**
     * @return DodgeReportDataDTO
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param DodgeReportDataDTO $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }
}
