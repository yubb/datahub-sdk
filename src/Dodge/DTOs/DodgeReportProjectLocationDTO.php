<?php

namespace Zlien\DataAccessService\Dodge\DTOs;

/**
 * Class DodgeReportProjectLocationDTO
 * @package Zlien\DataAccessService\Dodge\DTOs
 */
class DodgeReportProjectLocationDTO
{
    /**
     * @var string
     */
    public $addressLine1;

    /**
     * @var string
     */
    public $projectCounty;

    /**
     * @var string
     */
    public $fipsCounty;

    /**
     * @var string
     */
    public $cityName;

    /**
     * @var string
     */
    public $stateId;

    /**
     * @var string
     */
    public $zipCode;

    /**
     * @var string
     */
    public $zipCode5;

    /**
     * @var string
     */
    public $countryId;

    /**
     * @return string
     */
    public function getAddressLine1()
    {
        return $this->addressLine1;
    }

    /**
     * @param string $addressLine1
     */
    public function setAddressLine1($addressLine1)
    {
        $this->addressLine1 = $addressLine1;
    }

    /**
     * @return string
     */
    public function getProjectCounty()
    {
        return $this->projectCounty;
    }

    /**
     * @param string $projectCounty
     */
    public function setProjectCounty($projectCounty)
    {
        $this->projectCounty = $projectCounty;
    }

    /**
     * @return string
     */
    public function getFipsCounty()
    {
        return $this->fipsCounty;
    }

    /**
     * @param string $fipsCounty
     */
    public function setFipsCounty($fipsCounty)
    {
        $this->fipsCounty = $fipsCounty;
    }

    /**
     * @return string
     */
    public function getCityName()
    {
        return $this->cityName;
    }

    /**
     * @param string $cityName
     */
    public function setCityName($cityName)
    {
        $this->cityName = $cityName;
    }

    /**
     * @return string
     */
    public function getStateId()
    {
        return $this->stateId;
    }

    /**
     * @param string $stateId
     */
    public function setStateId($stateId)
    {
        $this->stateId = $stateId;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param string $zipCode
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
    }

    /**
     * @return string
     */
    public function getZipCode5()
    {
        return $this->zipCode5;
    }

    /**
     * @param string $zipCode5
     */
    public function setZipCode5($zipCode5)
    {
        $this->zipCode5 = $zipCode5;
    }

    /**
     * @return string
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * @param string $countryId
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;
    }
}
