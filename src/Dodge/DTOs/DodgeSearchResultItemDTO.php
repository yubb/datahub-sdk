<?php

namespace Zlien\DataAccessService\Dodge\DTOs;

/**
 * Class DodgeSearchResultItemDTO
 * @package Zlien\DataAccessService\Dodge\DTOs
 */
class DodgeSearchResultItemDTO
{
    /**
     * @var array
     */
    public $metaData;

    /**
     * @return array
     */
    public function getMetaData()
    {
        return $this->metaData;
    }

    /**
     * @param array $metaData
     */
    public function setMetaData($metaData)
    {
        $this->metaData = $metaData;
    }
}
