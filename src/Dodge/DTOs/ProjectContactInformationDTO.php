<?php

namespace Zlien\DataAccessService\Dodge\DTOs;

class ProjectContactInformationDTO
{
    /**
     * @var array
     */
    public $projectContacts;

    /**
     * @return array
     */
    public function getProjectContacts()
    {
        return $this->projectContacts;
    }

    /**
     * @param array $projectContacts
     */
    public function setProjectContacts($projectContacts)
    {
        $this->projectContacts = $projectContacts;
    }
}
