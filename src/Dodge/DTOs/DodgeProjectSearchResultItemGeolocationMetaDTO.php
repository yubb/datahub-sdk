<?php

namespace Zlien\DataAccessService\Dodge\DTOs;

/**
 * Class DodgeProjectSearchResultItemGeolocationMetaDTO
 * @package Zlien\DataAccessService\Dodge\DTOs
 */
class DodgeProjectSearchResultItemGeolocationMetaDTO
{
    /**
     * @var string
     */
    public $projectLatitude;

    /**
     * @var string
     */
    public $projectLongitude;

    /**
     * @var string
     */
    public $projectGeocodeLocationType;

    /**
     * @var string
     */
    public $projectGeocodeType;

    /**
     * @return string
     */
    public function getProjectLatitude()
    {
        return $this->projectLatitude;
    }

    /**
     * @param string $projectLatitude
     */
    public function setProjectLatitude($projectLatitude)
    {
        $this->projectLatitude = $projectLatitude;
    }

    /**
     * @return string
     */
    public function getProjectLongitude()
    {
        return $this->projectLongitude;
    }

    /**
     * @param string $projectLongitude
     */
    public function setProjectLongitude($projectLongitude)
    {
        $this->projectLongitude = $projectLongitude;
    }

    /**
     * @return string
     */
    public function getProjectGeocodeLocationType()
    {
        return $this->projectGeocodeLocationType;
    }

    /**
     * @param string $projectGeocodeLocationType
     */
    public function setProjectGeocodeLocationType($projectGeocodeLocationType)
    {
        $this->projectGeocodeLocationType = $projectGeocodeLocationType;
    }

    /**
     * @return string
     */
    public function getProjectGeocodeType()
    {
        return $this->projectGeocodeType;
    }

    /**
     * @param string $projectGeocodeType
     */
    public function setProjectGeocodeType($projectGeocodeType)
    {
        $this->projectGeocodeType = $projectGeocodeType;
    }
}
