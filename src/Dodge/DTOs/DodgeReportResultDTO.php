<?php

namespace Zlien\DataAccessService\Dodge\DTOs;

/**
 * Class DodgeReportResultDTO
 */
class DodgeReportResultDTO
{
    /**
     * @var DodgeErrorDTO
     */
    public $error;

    /**
     * @var DodgeReportDTO
     */
    public $report;

    /**
     * @var string
     */
    public $geoCode;

    /**
     * @return DodgeErrorDTO
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param DodgeErrorDTO $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }

    /**
     * @return DodgeReportDTO
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * @param DodgeReportDTO $report
     */
    public function setReport($report)
    {
        $this->report = $report;
    }

    /**
     * @return string
     */
    public function getGeoCode()
    {
        return $this->geoCode;
    }

    /**
     * @param string $geoCode
     */
    public function setGeoCode($geoCode)
    {
        $this->geoCode = $geoCode;
    }
}
