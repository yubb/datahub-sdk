<?php

namespace Zlien\DataAccessService\Dodge\DTOs;

/**
 * Class AddressLineDTO
 * @package Zlien\DataAccessService\Dodge\DTOs
 */
class AddressLineDTO
{
    /**
     * @var string
     */
    public $addressLine1;

    /**
     * @return string
     */
    public function getAddressLine1()
    {
        return $this->addressLine1;
    }

    /**
     * @param string $addressLine1
     */
    public function setAddressLine1($addressLine1)
    {
        $this->addressLine1 = $addressLine1;
    }
}
