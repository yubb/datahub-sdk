<?php

namespace Zlien\DataAccessService\Dodge;

/**
 * Class EnumDodgeConfig
 * @package Zlien\DataAccessService\Dodge
 */
abstract class EnumDodgeConfig
{
    /**
     * Whitelisting allowed headers
     */
    const ALLOWED_HEADERS = [
        'content-type',
        'cache-control',
        'license-nbr',
        'apiKey',
    ];
}
